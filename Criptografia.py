import random
import hashlib

# O Algoritmo de Euclides esta sendo utilizado para determinar o maior divisor comum.
def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

#O Algoritmo de Euclides tambem esta sendo utilizado para procurar a multiplicacao inversa dos dois numeros.
def multiplicative_inverse(e, phi):
    d = 0
    x1 = 0
    x2 = 1
    y1 = 1
    temp_phi = phi
    
    while e > 0:
        temp1 = temp_phi/e
        temp2 = temp_phi - temp1 * e
        temp_phi = e
        e = temp2
        
        x = x2- temp1* x1
        y = d - temp1 * y1
        
        x2 = x1
        x1 = x
        d = y1
        y1 = y
    
    if temp_phi == 1:
        return d + phi

#Teste para saber se o numero e primo.
def is_prime(num):
    if num == 2:
        return True
    if num < 2 or num % 2 == 0:
        return False
    for n in xrange(3, int(num**0.5)+2, 2):
        if num % n == 0:
            return False
    return True

def generate_keypair(p, q):
    if not (is_prime(p) and is_prime(q)):
        raise ValueError('Os dois numeros precisam ser primos.')
    elif p == q:
        raise ValueError('p e q nao podem ser igual')
    #n = pq
    n = p * q

    phi = (p-1) * (q-1)

    e = random.randrange(1, phi)

    g = gcd(e, phi)
    while g != 1:
        e = random.randrange(1, phi)
        g = gcd(e, phi)

    #Usando o Algoritmo de Euclides para gera a chave privada.
    d = multiplicative_inverse(e, phi)
    
    #Return public and private keypair
    #Public key is (e, n) and private key is (d, n)
    return ((e, n), (d, n))

def encrypt(pk, plaintext):
    #Compactando a chave junto com os componentes.
    key, n = pk
    #Convertendo cada letra em numero usando a^b.
    cipher = [(ord(char) ** key) % n for char in plaintext]
    #Retornando um array de bytes
    return cipher

def decrypt(pk, ciphertext):
    #Descompactando a chave junto com os componentes.
    key, n = pk
    #Gerando o texto e a chave usando a^b mod m.
    plain = [chr((char ** key) % n) for char in ciphertext]
    #Retornando um array de bytes como string
    return ''.join(plain)
    

if __name__ == '__main__':

    p = int(raw_input("Digite um numero primo (17, 19, 23, etc): "))
    q = int(raw_input("Digite outro numero primo (Not one you entered above): "))
    print "Gerando sua chave publica/privada . . ."
    public, private = generate_keypair(p, q)
    print "Sua chave publica e", public ," e a sua chave privada e", private
    message = raw_input("Coloque a mensagem a ser criptografada: ")
    encrypted_msg = encrypt(private, message)
    print "Sua mensagem criptografada e: "
    print ''.join(map(lambda x: str(x), encrypted_msg))
    print "Descriptando a mensagem", public ," . . ."
    print "Sua mensagem e:"
    print decrypt(public, encrypted_msg)